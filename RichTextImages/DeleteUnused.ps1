param
(
    [string]$dbServer = "",
    [string]$dbName = "",
    [string]$dbUser = "",
    [string]$dbPassword = "",
    [string]$awsProfile = "",
    [string]$bucketName = ""
)

function DeleteUnused-RichTextImages() {   
    $progress = 0;
    $failedCount = 0;
    $successCount = 0;
    $totalItems = 0;

    $results = $msmDatabase.ExecuteWithResults("
        SELECT
            filename, externalStorageProvider
        FROM
		    richTextImage
	    WHERE
		    DATEDIFF(s, richTextImage.createdOn, GETUTCDATE()) > 86400
		    AND NOT EXISTS (SELECT * FROM richTextImageUsage WHERE richTextImageUsage.fileName = richTextImage.fileName)
		    AND externalStorageProvider IS NOT NULL;
        SELECT @@ROWCOUNT AS totalItems;
    ")
    $totalItems = $results.Tables[1].totalItems
    $results.Tables[0] | ForEach-Object {
        $filename = $_.filename
        $externalStorageProvider = $_.externalStorageProvider | ConvertFrom-Json
       
        try {

            # lets do the actual delete for aws
            Remove-S3Object -BucketName $bucketName -Key $externalStorageProvider.ExternalStorageKey -Force | Out-Null

            # delete the row from msm		
			$msmDatabase.ExecuteNonQuery("
                DELETE FROM
		                richTextImage
	                WHERE
		                fileName = '$filename'
            ")
			
			$successCount++
        } catch {
            $failedCount++;
            echo "An exception occured while deleting $filename : $_"
        } finally {
            $progress++;
            $percentageComplete = ($progress / $totalItems) * 100
            Write-Progress -Activity "Delete in Progress" -Status "$percentageComplete% Complete:" -PercentComplete $percentageComplete;
        }
    }

    if ($totalItems -gt 0) {
        echo "Delete completed. Successfully deleted $successCount/$totalItems rows from aws!"
    } else {
        echo "No rows found to delete!"
    }
}

# ensure we're elevated
$isAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
if (!$isAdmin) {
    try {
        # we're not running elevated - so try to relaunch as administrator
        echo "Starting elevated PowerShell instance."
        $newProcess = New-Object System.Diagnostics.ProcessStartInfo "PowerShell"
        $newProcess.Arguments = @("-NoProfile","-NoLogo", $myInvocation.MyCommand.Definition, "-dbServer `"$dbServer`" -dbName `"$dbName`" -dbUser `"$dbUser`" -dbPassword `"$dbPassword`" -awsProfile `"$awsProfile`" -createBucket `"$createBucket`" -bucketName `"$bucketName`" -tables `"$tables`"")
        $newProcess.Verb = "runas"
        [System.Diagnostics.Process]::Start($newProcess)
    }
    catch {
        echo "Unable to start elevated PowerShell instance."
    }
    finally {
        # always exit this script either we're now running a separate elevated power shell or we've had an error
        exit
    }
}

[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]"Ssl3,Tls,Tls11,Tls12"

# ensure we have the Microsoft.SqlServer.Smo module installed
$sqlServerModules = Get-Module -ListAvailable -Name SqlServer

if ($sqlServerModules) {
    Import-Module SqlServer
}

[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null

if (!$sqlServerModules) {
    try {
        New-Object Microsoft.SqlServer.Management.SMO.Database | Out-Null
    } catch {
        echo "Installing SqlServer module..."
        Install-Module SqlServer -Scope CurrentUser
        Import-Module SqlServer
        [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null
    }
}

# ensure we have the AWS CLI installed
$cmdOutput = aws --version

if (!$cmdOutput.StartsWith("aws-cli")) {
    echo "AWS CLI not found! Please install AWS CLI and retry the operation. https://awscli.amazonaws.com/AWSCLIV2.msi"
    exit
}

# ensure we have the AWSPowerShell module installed
$awsPowerShellModules = Get-Module -ListAvailable -Name AWSPowerShell

if ($awsPowerShellModules) {
    Import-Module AWSPowerShell
}

if (!$awsPowerShellModules) {
    try {
        Set-AWSCredential
    } catch {
        echo "Installing AWSPowerShell module..."
        Install-Module AWSPowerShell -Scope CurrentUser
        Import-Module AWSPowerShell
    }
}

# check if we must use the default profile or the user specified profile as well as if it has a region set
$awsProfile = if (!$awsProfile) {"default"} else {$awsProfile}

if ((Get-AWSCredential -ListProfileDetail | Where-Object {$_.ProfileName -eq $awsProfile}).count -eq 1) {
    Set-AWSCredential -ProfileName $awsProfile # set default region for this session using the profiles region
    Set-DefaultAWSRegion (aws configure get region --profile $awsProfile)
    $region = (Get-DefaultAWSRegion) # set global region for access by functions 

    # ensure a default region has been set using the region found on the profile specified
    if ((Get-DefaultAWSRegion).count -eq 0) {
        echo "Please set a default region on the AWS profile you provided and try again"
        exit
    }

    echo "Using AWS profile: $awsProfile"
    echo "Using AWS region: $($region.Region)"
} else {
    echo "No profile found for $awsProfile, please specify -awsProfile with a valid profile or create the missing profile"
    exit
}

# check existing bucket exists and is valid
if($bucketName) {
    if ((Get-S3Bucket | Where-Object {$_.BucketName -eq $bucketName}).count -eq 0) {
        echo "Bucket $bucketName not found. Please specify a valid existing bucket name."
        exit
    }
} else {
    echo "-bucketName must be specified"
    exit
}

# ensure db credentials are specified
if (!$dbServer -or !$dbName -or !$dbUser) {
    echo "-dbServer, -dbName and -dbUser MUST be specified!"
    exit
}

$server = New-Object Microsoft.SqlServer.Management.SMO.Server(New-Object Microsoft.SqlServer.Management.Common.ServerConnection($dbServer, $dbUser, $dbPassword))
$msmDatabase = New-Object Microsoft.SqlServer.Management.SMO.Database($server, $dbName)

# lets go ahead and delete them
DeleteUnused-RichTextImages