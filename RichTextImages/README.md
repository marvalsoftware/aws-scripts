# AWS S3 RichTextImages clean up

A script which is used to clean up unused rich text images from the database as well as the AWS bucket

***

**Prerequisites:**

[AWS CLI](https://awscli.amazonaws.com/AWSCLIV2.msi) is installed and configured with a *default* or [named](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) profile and a [region](https://docs.aws.amazon.com/general/latest/gr/rande.html) has been set for this profile.
The script should also be run as an elevated user via *Run As Administrator* for the PowerShell instance.
It is to be noted that there might be a delay while AWSPowerShell module is installed (installed via the script itself), should it not be already.
***

**Usage:**

A complete example would be:

`.\DeleteUnused.ps1 -dbServer "localhost,1433" -dbName "MSM" -dbUser "sa" -dbPassword "123456" -awsProfile "default" -bucketName "msm-website-test"`

**Parameters:**

- *-dbServer* (string): The database server name optionally with a port separated by a comma i.e. *"localhost,1433"*
- *-dbName* (string): The name of the MSM database i.e. *"MSMProduction"*
- *-dbUser* (string): The database user with access to the above named database i.e. *"sa"*
- *-dbPassword* (string)\*: The password of the above database user, this parameter can be omitted should the password be blank i.e. *"1234"*
- *-awsProfile* (string)\*: The AWS profile configured via the AWS CLI, this parameter can be omitted should the *"default"* user be used i.e. *"ProductionProfile"*
- *-bucketName* (string): The name of the bucket, which already exists that should be used i.e. *"msm-website-test"*

\*These are optional parameters
