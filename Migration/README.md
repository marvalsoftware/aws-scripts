# AWS S3 Move Script

A script which is used to migrate data of various tables to Amazon S3 storage.

***

**Prerequisites:**

[AWS CLI](https://awscli.amazonaws.com/AWSCLIV2.msi) is installed and configured with a *default* or [named](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) profile and a [region](https://docs.aws.amazon.com/general/latest/gr/rande.html) has been set for this profile.
The script should also be run as an elevated user via *Run As Administrator* for the PowerShell instance.
It is to be noted that there might be a delay while AWSPowerShell module is installed (installed via the script itself), should it not be already.

For Minio (ie. Copying attachments to Azure).

The bucket name cooresponds to the Container within the Storage Account in Azure, this is used on the command line when using the script

The aws_access_key_id cooresponds to the storage account name, this is used with the command "aws configure" (the AWS CLI must be installed)

The aws_secret_access_key is the Storage Account Access Key, this is used with the command "aws configure" (the AWS CLI must be installed)

The AWS CLI can be downloaded from here: https://awscli.amazonaws.com/AWSCLIV2.msi

***

**Tables supported:**

- attachment
- queuedNotification
- note
- richTextImage

**Usage:**

A complete example would be (for AWS):

`.\MigrateToAWS.ps1 -dbServer "localhost,1433" -dbName "MSM" -dbUser "sa" -dbPassword "123456" -awsProfile "default" -bucketName "msm-website-test" -tables "attachment,queuedNotification"`

For Azure, use the switch 'isMinio', this will set the EndpointUrl to 'http://localhost:9000'

`.\MigrateToAWS.ps1 -isMinio 1 -dbServer "localhost,1433" -dbName "MSM" -dbUser "sa" -dbPassword "123456" -awsProfile "default" -bucketName "msm-website-test" -tables "attachment,queuedNotification"`

**Parameters:**

- *-dbServer* (string): The database server name optionally with a port separated by a comma i.e. *"localhost,1433"*
- *-dbName* (string): The name of the MSM database i.e. *"MSMProduction"*
- *-dbUser* (string): The database user with access to the above named database i.e. *"sa"*
- *-dbPassword* (string)\*: The password of the above database user, this parameter can be omitted should the password be blank i.e. *"1234"*
- *-awsProfile* (string)\*: The AWS profile configured via the AWS CLI, this parameter can be omitted should the *"default"* user be used i.e. *"ProductionProfile"*
- *-isMinio* (string)\*: If you are using Minio (for instance, using Azure instead of AWS), defaults to 0
- *-createBucket* (string): The name of the bucket to create which will subsequently be used for the moving of data, this can be omitted should a bucket already exist i.e. *"msm-website-test"*
- *-bucketName* (string): The name of the bucket, which already exists that should be used, this can be omitted if `-createBucket` is used i.e. *"msm-website-test"*
- *-tables* (string): A comma separated string of tables to move data for, the valid tables are listed under the **Tables supported** section above i.e. "attachment,queuedNotification"

\*These are optional parameters
